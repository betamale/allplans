All Plan Branches
============================

This plugin displays a new tab in Bamboo which shows all master plans + its branch plans on one page.
The goal of this plugin is to:

* quickly add/remove favourites
* have a total overview of what is failing
* easily filter out all successful plans