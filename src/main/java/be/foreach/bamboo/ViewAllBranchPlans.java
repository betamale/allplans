package be.foreach.bamboo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.acegisecurity.acls.Permission;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.ServletActionContext;
import com.atlassian.bamboo.build.CookieCutter;
import com.atlassian.bamboo.deployments.cache.LinkedDeploymentProject;
import com.atlassian.bamboo.deployments.cache.LinkedDeploymentProjectCacheService;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanExecutionManager;
import com.atlassian.bamboo.plan.PlanFavouriteService;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plan.branch.BranchDetectionService;
import com.atlassian.bamboo.plan.branch.VcsBranch;
import com.atlassian.bamboo.plan.cache.CachedPlanManager;
import com.atlassian.bamboo.plan.cache.ImmutableChainBranch;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import com.atlassian.bamboo.plan.cache.ImmutableTopLevelPlan;
import com.atlassian.bamboo.project.Project;
import com.atlassian.bamboo.project.ProjectManager;
import com.atlassian.bamboo.repository.RepositoryDefinition;
import com.atlassian.bamboo.security.BambooPermissionManager;
import com.atlassian.bamboo.security.acegi.acls.BambooPermission;
import com.atlassian.bamboo.user.BambooAuthenticationContext;
import com.atlassian.bamboo.ww2.BambooActionSupport;

public class ViewAllBranchPlans extends BambooActionSupport {
    private static final long serialVersionUID = -918128603693194379L;
    private final CachedPlanManager cachedPlanManager;
    private final ProjectManager projectManager;
    private final PlanFavouriteService planFavouriteService;
    private final TreeSet<String> branchNames = new TreeSet<String>();
    private final LinkedDeploymentProjectCacheService linkedDeploymentProjectCacheService;
    private BranchDetectionService branchDetectionService;
    private String currentlyFilteredBranch;

    public static final String cookieKeyFilteredBranch = "filteredBranch";

    private Map<Project, List<ImmutablePlan>> allBranchPlans = new LinkedHashMap<Project, List<ImmutablePlan>>();
    private Map<Project, List<ImmutableTopLevelPlan>> allUnbranchedPlans = new LinkedHashMap<Project, List<ImmutableTopLevelPlan>>();

    public ViewAllBranchPlans(
            ProjectManager projectManager,
            BambooAuthenticationContext bambooAuthenticationContext,
            CookieCutter cookieCutter,
            PlanFavouriteService planFavouriteService,
            BambooPermissionManager bambooPermissionManager,
            LinkedDeploymentProjectCacheService linkedDeploymentProjectCacheService,
            PlanExecutionManager planExecutionManager,
            CachedPlanManager cachedPlanManager,
            BranchDetectionService branchDetectionService) {
        this.projectManager = projectManager;
        this.planFavouriteService = planFavouriteService;
        this.cachedPlanManager = cachedPlanManager;
        this.setCookieCutter( cookieCutter );
        this.setAuthenticationContext( bambooAuthenticationContext );
        this.setBambooPermissionManager( bambooPermissionManager );
        this.linkedDeploymentProjectCacheService = linkedDeploymentProjectCacheService;
        this.setPlanExecutionManager( planExecutionManager );
        this.branchDetectionService = branchDetectionService;
    }

    @Override
    public String doDefault() throws Exception {
        allBranchPlans = new LinkedHashMap<Project, List<ImmutablePlan>>();
        allUnbranchedPlans = new LinkedHashMap<Project, List<ImmutableTopLevelPlan>>();
        HttpServletRequest request = ServletActionContext.getRequest();

        currentlyFilteredBranch = request.getParameter("filter");
        if(currentlyFilteredBranch == null) {
            currentlyFilteredBranch = cookieCutter.getValueFromCookie(cookieKeyFilteredBranch);
            HttpServletResponse response = ServletActionContext.getResponse();
            response.sendRedirect(response.encodeRedirectURL(request.getRequestURL() + "?filter=" + currentlyFilteredBranch));
        }
        cookieCutter.saveValueInCookie(cookieKeyFilteredBranch, currentlyFilteredBranch);

        List<Project> projects =  projectManager.getSortedProjects();
        for( Project project : projects ) {
            List<ImmutablePlan> branchPlanList = new ArrayList<ImmutablePlan>();
            List<ImmutableTopLevelPlan> unbranchedPlanList = new ArrayList<ImmutableTopLevelPlan>();
            List<ImmutableTopLevelPlan> topLevelPlans = cachedPlanManager.getPlansByProject( project );
            for( ImmutableTopLevelPlan topLevelPlan : topLevelPlans ) {
                ImmutablePlan relevantPlan = getRelevantPlanForTopLevelPlan( topLevelPlan );
                if(relevantPlan != null) {
                    branchPlanList.add( relevantPlan );
                }

                branchDetectionService.scheduleBranchDetectionForChain(topLevelPlan);
                List<VcsBranch> openBranches = branchDetectionService.getOpenBranches(topLevelPlan, null);
                if(openBranches != null){
                    for(VcsBranch openBranch: openBranches) {
                        String name = openBranch.getName();
                        branchNames.add(name);
                        if(name.equals(currentlyFilteredBranch) && relevantPlan == null) {
                            unbranchedPlanList.add(topLevelPlan);
                        }
                    }
                }
            }
            if( branchPlanList.size() > 0 ) {
                allBranchPlans.put( project, branchPlanList );
            }
            if( unbranchedPlanList.size() > 0 ) {
                allUnbranchedPlans.put(project, unbranchedPlanList);
            }
        }

        return "success";
    }

    @Override
    public boolean hasLinkedDeployments(PlanKey planKey) {
        if(planKey == null) {
            return false;
        } else {
            List<LinkedDeploymentProject> relatedDeployments = this.linkedDeploymentProjectCacheService.getRelatedDeployments(planKey);
            return !relatedDeployments.isEmpty();
        }
    }

    private ImmutablePlan getRelevantPlanForTopLevelPlan( ImmutablePlan topLevelPlan ) {
        if( StringUtils.isEmpty( currentlyFilteredBranch ) ) {
            return topLevelPlan;
        }

        List<ImmutableChainBranch> chainBranches = cachedPlanManager.getBranchesForChain(topLevelPlan);
        for( ImmutableChainBranch chainBranch : chainBranches ) {
            if(topLevelPlan.equals(chainBranch.getMaster())) {
                if( StringUtils.isNotEmpty(currentlyFilteredBranch) &&
                    chainBranch.getBuildName().equals(currentlyFilteredBranch) ) {
                        return chainBranch;
                }
            }
        }
        return null;
    }

    public Map<Project, List<ImmutablePlan>> getAllBranchPlans() {
        return allBranchPlans;
    }

    public Map<Project, List<ImmutableTopLevelPlan>> getAllUnbranchedPlans() {
        return allUnbranchedPlans;
    }

    public List<ImmutablePlan> getPlansForProject( Project project ) {
        return allBranchPlans.get( project );
    }

    public List<ImmutableTopLevelPlan> getUnbranchedPlansForProject(Project project) {
        return allUnbranchedPlans.get(project);
    }

    public boolean isFavourite( ImmutablePlan plan ) {
        return planFavouriteService.isFavourite( plan, getUser() );
    }

    public TreeSet<String> getBranchNames() {
        return branchNames;
    }

    public String getCurrentlyFilteredBranch() {
        return currentlyFilteredBranch;
    }

    public boolean hasPlanPermission( String permission, Plan plan ) {
        Permission bambooPermission = BambooPermission.buildFromName(permission);
        return bambooPermissionManager.hasPlanPermission( bambooPermission , plan.getPlanKey() );
    }
}
