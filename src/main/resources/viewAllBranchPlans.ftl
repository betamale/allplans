${webResourceManager.requireResource("be.foreach.bamboo.allplans:allplans-resources")}
[#import "displayWideBuildPlansList.ftl" as widePlanList]
<meta http-equiv="refresh" content="120"/>

[#compress]
<span id="selectBranchSpan">
    <label for="selectBranch">Select branch:</label>
    <select id="selectBranch" style="width:40%">
        <option value="">-MASTER-</option>
        [#list branchNames as branchName]
            <option value="${branchName}" [#if currentlyFilteredBranch?has_content && branchName == currentlyFilteredBranch] selected="selected" [/#if]>${branchName}</option>
        [/#list]
    </select>
    <script type="text/javascript">
        jQuery(document).ready(function() {
          jQuery("#selectBranch").select2();
        });
    </script>
</span>
<span>
    <input type="checkbox" id="hideSuccessful"/><label for="hideSuccessful">Hide 'Successful' builds</label>
    <input type="checkbox" id="hideNeverExecuted"/><label for="hideNeverExecuted">Hide 'Never built' and 'Suspended' builds</label>
    <input type="checkbox" id="hideUnknown"/><label for="hideUnknown">Hide 'Unknown' builds</label>
</span>
[#if allBranchPlans?has_content]
    [#list allBranchPlans?keys as project]
        [#assign projectPlans = action.getPlansForProject( project ) /]
        [#if projectPlans?has_content]
            <h2>Project: ${project.name}</h2>
            <div>
                [@widePlanList.displayWideBuildPlansList builds=projectPlans showProject=false /]
            </div>
        [/#if]
    [/#list]
[#else]
    <p>No plan branches on selected branch found.</p>
[/#if]
[/#compress]
[#if allUnbranchedPlans?has_content]
    [#compress]
    <div>
        <h3 id="createPlanBranchHeader">Add plan branch</h3>
        <p><small>(Click plan name link to branch selected plan.)</small></p>
        <ul class="createPlanBranchList" style="list-style-type:none">
        [#list allUnbranchedPlans?keys as project]
            [#assign projectPlans = action.getUnbranchedPlansForProject( project ) /]
            [#if projectPlans?has_content]
                <li>Project: ${project.name}</li>
                <ul class="createPlanBranchList" style="list-style-type:none">
                    [#list projectPlans as build]
                        <li>
                            <a onclick="createPlanBranch('${req.contextPath}','${build.key}','${currentlyFilteredBranch}')" href="#">
                                [#if build.master??]
                                    ${build.master.buildName}
                                [#else]
                                    ${build.buildName}
                                [/#if]
                            </a>
                        </li>
                    [/#list]
                </ul>
            [/#if]
        [/#list]
        </ul>
    </div>
    [/#compress]
[/#if]
