(function ($) {
    $(document).ready(function() {

        var initializing = true;

        var hideNeverExecuted = $('#hideNeverExecuted');
        hideNeverExecuted.change( function() {
            hideOrShowRow( '.Suspended, .NeverExecuted', !this.checked );

            if(!initializing) { setParameter(this); }
        });
        initializeFromParameter(hideNeverExecuted);

        var hideUnknown = $('#hideUnknown');
        hideUnknown.change( function() {
            hideOrShowRow( '.Unknown', !this.checked );

            if(!initializing) { setParameter(this); }
        });
        initializeFromParameter(hideUnknown);

        var hideSuccessful = $('#hideSuccessful');
        hideSuccessful.change( function() {
            hideOrShowRow( '.Successful', !this.checked );

            if(!initializing) { setParameter(this); }
        });
        initializeFromParameter(hideSuccessful);

        $('#selectBranch').change( function() {
            var branchName = $(this).val();
            if( branchName.indexOf( '-- ')==0 ) {
                branchName = '';
            }
            updateQueryStringParameter( "filter", branchName );
        });

        initializing = false;
    });

    function updateQueryStringParameter(key, value) {
        var uri = new RegExp("([^#]+)#?").exec(window.location.href)[1]; // strip filters
        var re = new RegExp("([?|&])" + key + "=.*?(&|$)", "i");
        var separator = uri.indexOf('?') !== -1 ? "&" : "?";
        if (uri.match(re)) {
            window.location.href = uri.replace(re, '$1' + key + "=" + value + '$2');
        }
        else {
            window.location.href = uri + separator + key + "=" + value;
        }
    }

    function getParameter(element) {
        var key = element.id.replace('#','');
        var uri = window.location.href;
        var re = new RegExp("[#&]" + key + "=([^&]*)&?", "i");

        var match = re.exec(uri);
        if(match == null)
            return element.checked;

        return match[1] == 'true';
    }

    function setParameter(element) {
        var key = element.id.replace('#','');
        var value = element.checked;
        var uri = window.location.href;
        var re = new RegExp("([#&])" + key + "=[^&]*(&?)", "i");
        var separator = uri.indexOf('#') !== -1 ? "&" : "#";
        if (uri.match(re)) {
            window.location = uri.replace(re, '$1' + key + "=" + value + '$2');
        }
        else {
            window.location = uri + separator + key + "=" + value;
        }
    }

    function initializeFromParameter(input){
        var element = input.get(0);
        input.prop('checked', getParameter(element)).change();
    }

    function isHidden( el ) {
        return $(el).css("display") == "none";
    }

    function hideOrShowRow( classes, show ) {
        var elements = $(classes).parent();
        var tables = elements.parent().parent();
        if( show ) {
            elements.show();
        } else {
            elements.hide();
        }

        tables.each( function( index, tableEl ) {
            var table = $(tableEl);
            var numberOfHiddenRows = 0;
            var allRows = table.children('tbody').children('tr');
            allRows.each( function( index, row ) {
                if( isHidden( row ) ) {
                    numberOfHiddenRows++;
                }
            });
            if( numberOfHiddenRows == allRows.size() ) {
                table.hide();
            } else {
                table.show();
            }

        } );

        tables.each( function( index, tableEl) {
            var table = $(tableEl);
            // Now also check if all tables for a project are hiding
            var parentDiv = table.parent( "div" );
            var allTables = parentDiv.children( "table" );
            var numberOfHiddenTables = 0;
            allTables.each( function( index, table ) {
                if( isHidden(table) ) {
                    numberOfHiddenTables++;
                }
            });
            if( numberOfHiddenTables == allTables.size() ) {
                parentDiv.prev().hide();
            } else {
                parentDiv.prev().show();
            }
        });

    }
})(jQuery);

function createPlanBranch(contextPath, topLevelPlanKey, branchName) {
    var url = contextPath + "/rest/api/latest/plan/" + topLevelPlanKey + "/branch/" + branchName + "?vcsBranch=" + branchName;
    console.log("createPlanBranch: url=" + url);
    var result =
        jQuery.ajax({
            url: url,
            type: "PUT",
            success:
                function(){
                    console.log("createPlanBranch: HTTP request succeeded!");
                    location.reload(true);
                }
        });
    console.log(result);
    return false;
}