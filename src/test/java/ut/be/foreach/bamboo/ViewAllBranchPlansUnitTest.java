package ut.be.foreach.bamboo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

import com.atlassian.bamboo.build.CookieCutter;
import com.atlassian.bamboo.deployments.cache.LinkedDeploymentProjectCacheService;
import com.atlassian.bamboo.plan.PlanExecutionManager;
import com.atlassian.bamboo.plan.PlanFavouriteService;
import com.atlassian.bamboo.plan.branch.BranchDetectionService;
import com.atlassian.bamboo.plan.branch.VcsBranch;
import com.atlassian.bamboo.plan.branch.VcsBranchImpl;
import com.atlassian.bamboo.plan.cache.CachedPlanManager;
import com.atlassian.bamboo.plan.cache.ImmutableChainBranch;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import com.atlassian.bamboo.plan.cache.ImmutableTopLevelPlan;
import com.atlassian.bamboo.project.Project;
import com.atlassian.bamboo.project.ProjectManager;
import com.atlassian.bamboo.security.BambooPermissionManager;
import com.atlassian.bamboo.user.BambooAuthenticationContext;
import com.opensymphony.xwork2.ActionContext;

import be.foreach.bamboo.ViewAllBranchPlans;

@RunWith (MockitoJUnitRunner.class)
public class ViewAllBranchPlansUnitTest
{
    @Mock
    private ProjectManager projectManager;
    private List<Project> projects = new ArrayList<Project>();
    @Mock
    private Project project = mock(Project.class);
    @Mock
    private BambooAuthenticationContext bambooAuthenticationContext;
    @Mock
    private CookieCutter cookieCutter;
    @Mock
    private PlanFavouriteService planFavouriteService;
    @Mock
    private BambooPermissionManager bambooPermissionManager;
    @Mock
    private LinkedDeploymentProjectCacheService linkedDeploymentProjectCacheService;
    @Mock
    private PlanExecutionManager planExecutionManager;
    @Mock
    private CachedPlanManager cachedPlanManager;
    private List<ImmutableTopLevelPlan> plans = new ArrayList<ImmutableTopLevelPlan>();
    @Mock
    private ImmutableTopLevelPlan plan1;
    @Mock
    private ImmutableTopLevelPlan plan2;
    @Mock
    private ImmutableTopLevelPlan plan3;
    @Mock
    private ImmutableChainBranch branchPlan1;
    private List<ImmutableChainBranch> chainBranchList = new ArrayList<ImmutableChainBranch>();
    @Mock
    private BranchDetectionService branchDetectionService;

    @InjectMocks
    private ViewAllBranchPlans viewAllBranchPlans;

    @Mock
    private HttpServletRequest httpRequest;

    private VcsBranch vcsBranch = new VcsBranchImpl("branch1");
    private List<VcsBranch> openBranches = new ArrayList<VcsBranch>();

    @Before
    public void setUp() {
        projects.add(project);
        when(projectManager.getSortedProjects()).thenReturn(projects);

        plans.add(plan1);
        plans.add(plan2);
        when(cachedPlanManager.getPlansByProject(project)).thenReturn(plans);
        when(branchPlan1.getBuildName()).thenReturn("branch1");
        when(branchPlan1.getMaster()).thenReturn(plan1);
        chainBranchList.add(branchPlan1);
        when(cachedPlanManager.getBranchesForChain(plan1)).thenReturn(chainBranchList);

        ServletActionContext.setContext(new ActionContext(new LinkedHashMap<String, Object>()));
        when(httpRequest.getParameter("filter")).thenReturn("branch1");
        ServletActionContext.setRequest(httpRequest);

        openBranches.add(vcsBranch);
        when(branchDetectionService.getOpenBranches(plan1, null)).thenReturn(openBranches);
        when(branchDetectionService.getOpenBranches(plan2, null)).thenReturn(openBranches);
    }

    @Test
    public void testDoDefault() throws Exception
    {
        assertEquals("success", viewAllBranchPlans.doDefault());
        Map<Project, List<ImmutablePlan>> allBranchPlans = viewAllBranchPlans.getAllBranchPlans();
        assertNotNull(allBranchPlans.get(project));
        assertEquals(1, allBranchPlans.get(project).size());
        assertEquals(branchPlan1, allBranchPlans.get(project).get(0));
        Map<Project, List<ImmutableTopLevelPlan>> allUnbranchedPlans = viewAllBranchPlans.getAllUnbranchedPlans();
        TreeSet<String> branchNames = viewAllBranchPlans.getBranchNames();
        assertEquals(1, branchNames.size());
        assertEquals("branch1", branchNames.first());
        assertEquals("branch1", viewAllBranchPlans.getCurrentlyFilteredBranch());
        assertEquals(1, allUnbranchedPlans.size());
        assertNotNull(allUnbranchedPlans.get(project));
        assertEquals(1, allUnbranchedPlans.get(project).size());
        assertEquals(plan2, allUnbranchedPlans.get(project).get(0));
    }
}