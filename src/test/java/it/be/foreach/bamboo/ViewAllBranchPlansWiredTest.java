package it.be.foreach.bamboo;

import be.foreach.bamboo.ViewAllBranchPlans;

import org.junit.Test;
import org.junit.runner.RunWith;
import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import com.atlassian.sal.api.ApplicationProperties;

import static org.junit.Assert.assertEquals;

@RunWith(AtlassianPluginsTestRunner.class)
public class ViewAllBranchPlansWiredTest
{
    private final ApplicationProperties applicationProperties;
    private final ViewAllBranchPlans viewAllBranchPlans;

    public ViewAllBranchPlansWiredTest(ApplicationProperties applicationProperties,ViewAllBranchPlans viewAllBranchPlans)
    {
        this.applicationProperties = applicationProperties;
        this.viewAllBranchPlans = viewAllBranchPlans;
    }

    @Test
    public void testDoDefault() throws Exception
    {
        viewAllBranchPlans.doDefault();
        //assertEquals("Something is wrong!", "1", "1");
    }
}